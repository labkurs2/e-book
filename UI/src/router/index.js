import Vue from "vue";
import VueRouter from "vue-router";
import Home from "../views/Home.vue";
import Originals from "../views/Originals";
import Genres from "../views/Genres";
import Popular from "../views/Popular";
import Canvas from "../views/Canvas";
import Api from "../views/Api";
import CreateSeries from "../views/CreateSeries";
import CreateEpisodes from "../views/CreateEpisodes";
import testJwt from "../views/testJWT";
import testApiPerXonin from "../views/testApiPerXonin";
import login from "../views/login";
import Signup from "../views/Signup";
import popular from "../views/Popular";
import canvas from "../views/Canvas";
import Episode from "../views/Episode";

Vue.use(VueRouter);

const routes = [
  {
    path: "/",
    name: "Home",
    component: Home
  },
  {
    path: "/about",
    name: "About",
    // route level code-splitting
    // this generates a separate chunk (about.[hash].js) for this route
    // which is lazy-loaded when the route is visited.
    component: () =>
      import(/* webpackChunkName: "about" */ "../views/About.vue")
  },
  {
    path: "/originals",
    name: "Originale",
    component: Originals
  },
  {
    path: "/genres",
    name: "Genres",
    component: Genres
  },

  {
    path: "/popular",
    name: "Popular",
    component: Popular
  },

  {
    path: "/Canvas",
    name: "Canvas",
    component: Canvas
  },

  {
    path: "/api",
    name: "Api",
    component: Api
  },
  {
    path: "/create/series",
    name: "Series",
    component: CreateSeries
  },
  {
    path: "/create/episodes",
    name: "Episodes",
    component: CreateEpisodes
  },
  {
    path: "/jwt",
    name: "Jwt",
    component: testJwt
  },
  {
    path: "/apixonit",
    name: "apixonit",
    component: testApiPerXonin
  },
  {
    path: "/login",
    name: "Login",
    component: login
  },
  {
    path: "/signup",
    name: "Signup",
    component: Signup
  },
  {
    path: "/episode",
    name: "Episode",
    component: Episode
  },
  {
    path: "/popular",
    name: "Popular",
    component: popular
  },
  {
    path: "/canvas",
    name: "Canvas",
    component: canvas
  }
 

];

const router = new VueRouter({
  mode : 'history',
  routes
});

export default router;
